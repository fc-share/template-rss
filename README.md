# How to use  
#### Javascript
```js
  var rssTpl = "";  
  rssTpl += '<div class="row">';  
      rssTpl += '<dl class="clearfix">';  
          rssTpl += '<dt class="rss-img">{IMG}</dt>';  
            rssTpl += '<dd class="rss-date">{YEAR}.{MONTH}.{DAY}</dd>';  
          rssTpl += '<dt class="rss-tit"><a href="{URL}" target="_blank">{DESC}</a></dd>';  
      rssTpl += '</dl>';  
  rssTpl += '</div>';  
  FCBase.rss.data = [{
      selector: "#feed",  
      url: 1,  
      total: 3,  
      maxTitle: 40,  
      maxDesc: 100,  
      ImageSize: {width: "290", height: "160"}, // Default: 300 x 200  
      endText: "...",  
      noImage: "https://via.placeholder.com/300x200.png?text=Test+No+Image", // No Image  
      template: rssTpl  
  }];  
```
## Shortcode 1
| ShortCode |            Default            |                                            Description                                            |  
|:---------:|:-----------------------------:|:-------------------------------------------------------------------------------------------------:|  
|  selector |               id              | Nơi sẽ chèn RSS vào thường đặt theo ID hoặc class                                                 |  
|    url    |             number            | Số thứ tự của URL, xem setting tại `shared/rss/rss.config.php` |  
|   total   |             number            | Tổng số bài viết muốn hiển thị |  
|  maxTitle |             number            | Giới hạn kí tự trên tiêu đề |  
|  maxDesc  |             number            | Giới hạn kí tự trên mô tả |  
| ImageSize | {width: "xxx", height: "xxx"} | Kích thức **image**, nếu không nhập giá trị, kích thước mặt định `width: 300px;` `height: 200px;` |  
|  endText  |             symbol            | Xuất hiện ở cuối khi tổng số kí tự `maxTitle` và `maxDesc` vượt quá giới hạn                      |  
|  noImage  |              link             | Dùng để chèn khi bài viết không có hình ảnh                                                       |  
| template  |             rssTpl            | Dùng để tuỳ chỉnh lại Template RSS                                                                |  
  
  
## Shortcode 2
  
|                Shortcode               |       Default      |                                 Description                                 |   Example  |
|:--------------------------------------:|:------------------:|:---------------------------------------------------------------------------:|:----------:|
|                  YEARS                 |        xxxx        | Định dạng năm theo dạng 4 số                                                |    2019    |
|                 MONTHS                 |         xx         | Định dạng tháng theo dạng 2 số                                              |     02     |
|               MONTHS-LONG              |                    | Định dạng tháng theo dạng chữ cái                                           |   October  |
|              MONTHS-SHORT              |                    | Định dạng tháng theo dạng chữ cái viết tắt                                  |     Oct    |
|              WEEKDAYS-LONG             |                    | Định dạng tuần theo dạng chữ cái                                            |  Thursday  |
|             WEEKDAYS-SHORT             |                    | Định dạng tuần theo dạng chữ cái viết tắt                                   |     Thu    |
|                  DAYS                  |         xx         | Định dạng ngày theo dạng 2 số                                               |     01     |
|                  HOURS                 |         xx         | Định dạng giờ theo dạng 2 số và theo chu kì **12 giờ (AM/PM)**              |    01 AM   |
|                 MINUTES                |         xx         | Định dạng phút theo dạng 2 số                                               |     05     |
|                 SECONDS                |         xx         | Định dạng giây theo dạng 2 số                                               |     03     |
|                  DATE                  |   year-month-day   | Định dạng ngày theo dạng `năm - tháng - ngày`                               | 2019-11-02 |
|                  TIME                  | hour:minute:second | Định dạng thời gian theo dạng: `giờ : phút : giây`                          |  11:30:02  |
|         URL / LINK / PERMALINK         |        link        | Lấy được link bài viết của blog                                             |            |
|        USERS / AUTHORS / CREATOR       |       writer       | Lấy được tên tác giả đăng bài                                               |    admin   |
|       CAT / CATEGORY / CATEGORIES      |                    | Lấy được danh mục của bài viết                                              |            |
|              TITLE / NAME              |                    | Lấy được tiêu đề bài viết. Ràng buộc bởi **maxTitle** và **endText**        |            |
|           DESC / DESCRIPTION           |                    | Lấy được đoạn mô tả ngắn của bài viết. Ràng buộc **maxDesc** và **endText** |            |
|         CONTENT / POST / DETAIL        |                    | Lấy được nội dung bài viết                                                  |            |
| IMG / IMAGE / PHOTO / THUMB /THUMBNAIL |                    | Lấy link hình ảnh                                                           |            |

## Changelog
 - 2019/11/13: Fix post content & description limit
 - 2019/10/07: Fix background image